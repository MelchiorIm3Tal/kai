import hashlib

import pytest

from src.keeshond import file_hash_check


# Calculate correct hash for regular text file
def test_calculate_hash_regular_file(tmp_path):
    # Create a test file with known content
    test_file = tmp_path / "test.txt"
    test_content = b"test content"
    test_file.write_bytes(test_content)

    # Calculate expected hash
    expected_hash = hashlib.blake2s()
    expected_hash.update(test_content)
    expected = expected_hash.hexdigest()

    # Calculate actual hash
    actual = file_hash_check.calculate_hash(str(test_file))

    assert actual == expected


# Handle file path that doesn't exist
def test_calculate_hash_nonexistent_file():
    with pytest.raises(FileNotFoundError):
        file_hash_check.calculate_hash("nonexistent_file.txt")


# File exists and hash matches previous hash returns (0, current_hash)
def test_matching_hash_returns_zero_and_hash(mocker):
    test_file = "test.txt"
    test_hash = "abc123"

    mocker.patch('os.path.exists', return_value=True)
    mocker.patch('src.keeshond.file_hash_check.calculate_hash', return_value=test_hash)

    result = file_hash_check.check_file(test_file, test_hash)

    assert result == (0, test_hash)

    # File does not exist returns (15, empty string)


def test_nonexistent_file_returns_error_code(mocker):
    test_file = "nonexistent.txt"
    test_hash = "abc123"

    mocker.patch('os.path.exists', return_value=False)

    result = file_hash_check.check_file(test_file, test_hash)

    assert result == (15, "")
